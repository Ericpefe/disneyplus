import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes, Navigate } from "react-router-dom";
import EsEs from "./Components/EsEs/EsEs"
import Login from "./Components/Login/Login"
import Sub from "./Components/Sub/Sub"
import Home from "./Components/Home/Home";
import Display from "./Components/Display/Display";
import DB from "./Components/DB";
import Search from "./Components/Search/Search";
import Watchlist from "./Components/Watchlist/Watchlist";
import Original from "./Components/Home/Original";
import Movie from "./Components/Home/Movie";
import Serie from "./Components/Home/Serie";
import "./App.css"


function App () { 

   const [user, setUser] = useState(false)


  const PrivateRoute = ({ children}) => {
        
    if (user) {
      return children
    }
      
    return <Navigate to="/" />
  }
   
const checkUser = () => {
   if (user){
     return <Navigate to="/home" />
   }else {
     return <EsEs />
   }
}
    
        return(
          <div className='App'>
             <Router>
        <Routes>
          <Route path="/" element={checkUser()} />
          <Route path="/login" element={<Login user={user} setUser={setUser} />} />
          <Route path="/subscribe" element={<Sub />} />
          <Route
          path="/home"
          element={
            <PrivateRoute>
              <Home setUser={setUser} />
            </PrivateRoute>
          }
        />
          <Route
          path="/home/:name"
          element={
            <PrivateRoute>
              <Display setUser={setUser} DB={DB} />
            </PrivateRoute>
          }
        />
           <Route
          path="/home/search"
          element={
            <PrivateRoute>
              <Search setUser={setUser} DB={DB} />
            </PrivateRoute>
          }
        />
          <Route
          path="/home/watchlist"
          element={
            <PrivateRoute>
              <Watchlist setUser={setUser} DB={DB} />
            </PrivateRoute>
          }
        />
           <Route
          path="/home/original"
          element={
            <PrivateRoute>
              <Original setUser={setUser} DB={DB} />
            </PrivateRoute>
          }
        />
           <Route
          path="/home/movie"
          element={
            <PrivateRoute>
              <Movie setUser={setUser} DB={DB} />
            </PrivateRoute>
          }
        />
          <Route
          path="/home/serie"
          element={
            <PrivateRoute>
              <Serie setUser={setUser} DB={DB} />
            </PrivateRoute>
          }
        />
        </Routes>
      </Router>

    
  

        <footer>
         <img className="logoFooter" alt="disney+ logo" src="https://cnbl-cdn.bamgrid.com/assets/7ecc8bcb60ad77193058d63e321bd21cbac2fc67281dbd9927676ea4a4c83594/original"></img>
         <div className="links-footer">
         <a href="/" className="footer">Acuerdo de suscripción</a>
         <a href="/" className="footer">Política de privacidad</a>
         <a href="/" className="footer">Derechos de la privacidad de la UE y del Reino Unido</a>
         <a href="/" className="footer">Política de cookies</a>
         <a href="/" className="footer">Dispositivos compatibles</a>
         <a href="/" className="footer">Ayuda</a>
         <a href="/" className="footer">¿Quiénes somos?</a>
         <a href="/" className="footer">Administrar preferencias</a>
         </div>
         <p className="text-footer">© 2021 Disney y entidades relacionadas. Reservados todos los derechos.</p>
       </footer>
    
          </div>
          )
          }



export default App