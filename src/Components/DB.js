const DB = [
    {
        name: "encanto",
        title: "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/B17197D11198CF1F17C4F1DE29F9BFC43313F59B5462382823696DB3B02F14D2/scale?width=1440&aspectRatio=1.78&format=png",
        category: "Familiar, Fantastica, Animación, Musical",
        sinopsis: "Cuenta la historia de una familia extraordinaria, los Madrigal, que viven escondidos en las montañas de Colombia, en una casa mágica situada en un enclave maravilloso llamado Encanto. La magia de Encanto ha dotado a todos los niños de la familia un don único, desde la súperfuerza hasta el poder de curar... Pero se olvidó de un miembro de la familia: Mirabel. Cuando ésta descubre que la magia que rodea Encanto está en peligro, decide que ella, la única Madrigal normal, podría ser la última esperanza de su extraordinaria familia.",
        director: "Jared Bush, Byron Howard, Charise Castro Smith",
        type: 'movie',
        original: false,
        year: 2021,
        runtime: "1 h 49 min",
        img: "http://t1.gstatic.com/images?q=tbn:ANd9GcSHskZ06fUYhjWlD6DfQgFGNn-yDeIlbIOy22557QyDMEEi2Ta7",
        trailer: <iframe width="560" height="315" className="trailer" src="https://www.youtube.com/embed/SAH_W9q_brE" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    },

    {
        name: "greyanatomy",
        title: "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/68EF4693995453EA76BDD7E4510970C7D520D891BA1170DE9E0B309500A4CCE9/scale?width=1440&aspectRatio=1.78&format=png",
        category: "Medicina, Drama, Telenovela/Melodrama, Investigación",
        sinopsis: "La serie se centra en la vida de los cirujanos internos, residentes y especialistas a medida de que se convierten en médicos cirujanos experimentados mientras tratan de equilibrar sus relaciones personales y profesionales.",
        director: "Shonda Rhimes",
        type: 'serie',
        original: false,
        year: "2005-2021",
        runtime: "18 Temporadas",
        img: "https://static.klisst.com/4338141a-c6c8-4eca-aab7-866abb2d9984.jpeg",
        trailer: <iframe width="560" height="315" className="trailer" src="https://www.youtube.com/embed/VftMccvBNkE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: "dugcarl",
        title: "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/899E23AAB6DE0B84EF2BDC8B866BF54F948B0C6E060AA58C6D2D54A5FDC38510/scale?width=1440&aspectRatio=1.78&format=png",
        category: "Familiar, comedia, animación",
        sinopsis: "Narra las aventuras de Dug, el perro de la película 'Up', mientras descubre los peligros de los suburbios.",
        director: "Bob Peterson",
        type: 'serie',
        original: true,
        year: 2021,
        runtime: "5 episodios",
        img: "https://ocioworld.files.wordpress.com/2021/09/scale.jpg",
        trailer: <iframe width="560" height="315" className="trailer" src="https://www.youtube.com/embed/jgvmT_aeZac" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>,
    },

    {
        name: "eternals",
        title: "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/998140E8ED2E3A7644484D564C861D61CFFCCCE204BD2A64A7C421905C8B3ECB/scale?width=1440&aspectRatio=1.78&format=png",
        category: "Ciencia ficción, Superhéroe, Acción y aventura.",
        sinopsis: "Hace millones de años, los seres cósmicos conocidos como los Celestiales comenzaron a experimentar genéticamente con los humanos. Su intención era crear individuos superpoderosos que hicieran únicamente el bien, pero algo salió mal y aparecieron los Desviantes, destruyendo y creando el caos a su paso. Ambas razas se han enfrentado en una eterna lucha de poder a lo largo de la historia. En medio de esta guerra, Ikaris y Sersi tratarán de vivir su propia historia de amor.",
        director: "Chloé Zhao",
        type: 'movie',
        original: false,
        year: 2021,
        runtime: "2 h 37 min",
        img: "https://sm.ign.com/ign_latam/screenshot/default/enelprincipio_r2w1.jpg",
        trailer: <iframe width="560" height="315" className="trailer" src="https://www.youtube.com/embed/f7aFqw3eYSs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: "librobobafett",
        title: "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/4012680892DCA8C3CAFED4745F34F073342E32B0A18D0251F9B3DF16D99D6144/scale?width=1440&aspectRatio=1.78&format=png",
        category: "Ciencia ficción, Acción y Aventura",
        sinopsis: "Spin-off de 'The Mandalorian' centrado en el cazarecompensas Boba Fett y su lugarteniente Fennec Shand, que vuelven al planeta Tatooine lidiando con el inframundo galáctico para reclamar el territorio que en su día gobernó Jabba el Hutt.",
        director: "Jon Favreau",
        type: 'serie',
        original: true,
        year: 2021,
        runtime: "1 temporada",
        img: "https://www.starwarsnewsnet.com/wp-content/uploads/2021/11/FEAXvmoXEAoIWfE.jpg",
        trailer: <iframe width="560" height="315" className="trailer" src="https://www.youtube.com/embed/CDMo4xDbzjo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: "rescate",
        title: "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/0F519D2ABA0E97B483F044A8809C64DC42D85F5B622B240FCC8530C8A57CF715/scale?width=1440&aspectRatio=1.78&format=png",
        category: "Supervivencia, Documental",
        sinopsis: "'The Rescue' retrata el dramático rescate de 2018 de 12 chicos tailandeses y su entrenador de fútbol, atrapados dentro de una cueva inundada. Los directores y productores ganadores del Óscar E. Chai Vasarhelyi y Jimmy Chin ponen a descubierto el peligroso mundo del buceo en cuevas, la valentía de los rescatadores y la dedicación de una comunidad entera que hizo grandes sacrificios para salvar a estos jóvenes. Una excursión para explorar un sistema cercano de cuevas tras un partido de fútbol se transformó en una epopeya de dos semanas de supervivencia y una historia que llamaría la atención del mundo entero. Con acceso exclusivo e imágenes nunca antes vistas, la película cuenta la historia de la imaginación, la determinación y el trabajo en equipo sin precedentes que se desplegaron durante esta heroica misión al borde del abismo con riesgos de vida o muerte.",
        director: "Elizabeth Chai Vasarhelyi, Jimmy Chin",
        type: 'serie',
        original: true,
        year: 2021,
        runtime: "1 h 47 min",
        img: "https://i0.wp.com/cuatrobastardos.com/wp-content/uploads/2021/12/The-Rescue-Poster.jpg?resize=1080%2C1350&ssl=1",
        trailer: <iframe width="560" height="315" className="trailer" src="https://www.youtube.com/embed/raVceiFQSjw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'cenicienta',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/1FF4B263A540D3B4D0F951A16F3521A4D1C738738F73FF63E3F2628B090B391F/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Romance, Familiar, Fantásticas, Animación, Musical',
        sinopsis: 'Cenicienta era una hermosa y bondadosa joven, a quien su cruel madrastra y sus dos hermanastras obligaban a ocuparse de las labores más duras del palacio, como si fuera la última de las criadas. Sucedió que el hijo del Rey celebró un gran baile. Cenicienta ayudó a sus egoístas hermanastras a vestirse y peinarse para la fiesta. Cuando se hubieron marchado, la pobre niña se echó a llorar amargamente porque también le hubiera gustado ir al baile. Pero hete aquí que su hada madrina le hizo una carroza con una calabaza, convirtió seis ratoncitos en otros tantos caballos, una rata en un grueso cochero, y seis lagartos en elegantes lacayos. Después tocó a Cenicienta con su varita mágica y sus harapos se convirtieron en un vestido resplandeciente, y sus alpargatas en preciosos zapatitos de cristal. Pero el hada advirtió a Cenicienta que a medianoche, todo volvería a ser como antes. Cuando llegó a la fiesta, su radiante belleza causó asombro y admiración. El Príncipe no se apartó de ella ni un solo instante. Pero la pobre Cenicienta, tan feliz con su Príncipe, se olvidó de que a las doce terminaba el hechizo. Cuando oyó la primera campanada de la medianoche, echó a correr y perdió uno de sus zapatos de cristal.',
        director: 'Clyde Geronimi, Hamilton Luske, Wilfred Jackson',
        type: 'movie',
        original: false,
        year: 1950,
        runtime: '1 h 15 min',
        img: 'https://www.misfiestas.es/wp-content/uploads/2017/05/cinderella-top-banner.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/VsxWIoby2cA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'hercules',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/37B7EF20B4EA71EBDBFDC754B7C04C602F0E7F612D8E72AEBFB17ADCF16E5624/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'De adolescente a adulto, Familiar, Comedia, Animación, Acción y aventura',
        sinopsis: 'Hércules, el hijo de Zeus y de Alcmena, reina de Tebas, es secuestrado del Olimpo por los secuaces de Hades, que tienen la misión de despojarlo de su inmortalidad. Hércules crecerá en un mundo de mortales hasta que le llegue la hora de regresar al Olimpo.',
        director: 'John Musker, Ron Clements',
        type: 'movie',
        original: false,
        year: 1997,
        runtime: '1h 33 min',
        img: 'https://muzikspeaks.files.wordpress.com/2015/07/hercules-banner.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/ZvtspevZxpg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'libroselva',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/74850685DC96EF9F2B5B4136A6DEDADC544B7EDF1FE59EA6050D60420D57C290/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Familiar, Animación, Acción y aventura.',
        sinopsis: 'Tras la muerte de sus padres, Mowgli, un niño de apenas dos años, queda abandonado en la selva y es recogido por una manada de lobos. En el seno de la manada, Mowgli es criado como un lobo más hasta que crece y empieza a desenvolverse por sí mismo en la selva.',
        director: 'Wolfgang Reitherman',
        type: 'movie',
        original: false,
        year: 1967,
        runtime: '1 h 18 min',
        img: 'http://3.bp.blogspot.com/-YJDd8uemRjE/UYts4L14W2I/AAAAAAAA67o/459YW1K0vs4/s1600/The-Jungle-Book-poster+cartel+high+quality+blu+ray+trailer+dvd+platinum+diamond+edition+walt+disney+1967+2013+el+libro+de+la+selva+mowgli+baloo+bagheera+shere+khan+kaa+rey+louie+banner.png',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/kLJhKCfxecw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'sirenita',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/6D52697B1301A66D9C74328C27BF5F4638D970134F289286B5FF4744A10EC0FF/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Romance, Familiar, Fantásticas, Animación, Musical.',
        sinopsis: 'Ariel, hija del rey Tritón, es la princesa de las sirenas. Está a punto de celebrarse su fiesta de cumpleaños y su mayor ilusión es poder conocer a los seres humanos. Con la ayuda de la bruja Úrsula, Ariel consigue subir a la superficie, donde salva de morir ahogado a un hermoso príncipe, cuyo barco acaba de naufragar, del que se enamora perdidamente.',
        director: 'John Musker, Ron Clements',
        type: 'movie',
        original: false,
        year: 1989,
        runtime: '1h 19 min',
        img: 'https://www.agendamenuda.es/images/0-que-hacer/2017/cine/la-sirenita.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/tEugAy1kXKQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'belladurmiente',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/E4534832F13CEFC0B49A345C15C69FA11E6949F4F3AC033EE95EAE3A131793B2/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Romance, Familiar, Fantásticas, Animación',
        sinopsis: 'Había una vez dos reyes que esperaban con alegría el nacimiento de su primera hija, a la que llamarían Aurora. Para celebrarlo, organizaron una fiesta a la que invitaron a todos los habitantes del reino. Pero olvidaron invitar a la malvada bruja Maléfica que, enfurecida, lanzó un terrible hechizo sobre la princesa: el día de su décimosexto cumpleaños, se pincharía con el uso de una rueca y moriría. Pero sus tres divertidas hadas madrinas descubren una forma de romper el maleficio: no morirá, permanecerá dormida hasta que un valiente príncipe la bese...',
        director: 'Clyde Geronimi',
        type: 'movie',
        original: false,
        year: 1959,
        runtime: '1 h 15 min',
        img: 'https://tvnotiblog.com/baulpop/wp-content/uploads/2020/05/la-bella-durmiente-pelicula.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/P7OCyY1CvwE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'frozen',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/BA220660FFEA0D04871D859FB0042F91B11E920384F69D23332E1445FDAC5D7D/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Familiar, Fantásticas, Infantil, Animación, Musical.',
        sinopsis: 'Cuando una profecía condena a un reino a vivir un invierno eterno, la joven Anna, el temerario montañero Kristoff y el reno Sven emprenden un viaje épico en busca de Elsa, hermana de Anna y Reina de las Nieves, para poner fin a un gélido hechizo... Adaptación libre del cuento "La reina de las nieves".',
        director: 'Chris Buck, Jennifer Lee',
        type: 'movie',
        original: false,
        year: 2013,
        runtime: '1 h 38 min',
        img: 'https://blogs.eitb.eus/estrenosdecine/wp-content/uploads/sites/31/2014/09/frozen-banner-final1.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/QTvcYow0Z5U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'delreves',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/47E9FD34552327FA5D12EB6271155CD6556E5235C7C4C2FBC37B2A6872C192A2/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'De adolescente a adulto, Familiar, Animación',
        sinopsis: 'Riley es una chica que disfruta o padece toda clase de sentimientos. Aunque su vida ha estado marcada por la Alegría, también se ve afectada por otro tipo de emociones. Lo que Riley no entiende muy bien es por qué motivo tiene que existir la Tristeza en su vida. Una serie de acontecimientos hacen que Alegría y Tristeza se mezclen en una peligrosa aventura que dará un vuelco al mundo de Riley.',
        director: 'Pete Docter, Ronaldo Del Carmen',
        type: 'movie',
        original: false,
        year: 2015,
        runtime: '1 h 24 min',
        img: 'http://4.bp.blogspot.com/-G44luHloOCU/VapgT6XiltI/AAAAAAAAD1g/Fqt4Kwjhf2A/s1600/del_reves_banner_0%2B%2528600%2529.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/ZOWV9F7LnIQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'monstruosobra',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/0D16EDB4A24681A27FAD25B768481791DD4D13FDD26ABAE5AECA237048EA3EC6/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Familiar, Comedia, Animación',
        sinopsis: 'El joven Tylor Tuskmon acaba de graduarse en la facultad de sustos de Monsters University, pero cuando llega a Monstruos S.A. descubre que desde ese mismo día ya no trabajan con sustos, sino que con risas, mientras Mike y Sully, están ahora al mando. Continuación de la franquicia "Monstruos S.A.", en este caso en formato serie.',
        director: 'Roberts Gannaway',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://i0.wp.com/hipertextual.com/wp-content/uploads/2021/06/monstruos_a_la_obra_pete_docter.jpg?fit=1600%2C1067&ssl=1',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/SiX-KGaS5xE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'onward',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/17B5120922BE08320E6084F8B2289DEDF9E3D90284561F91168325E358802D80/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Familiar, Comedia, Fantásticas, Animación, Acción y Aventura.',
        sinopsis: 'Ambientado en un mundo de fantasía suburbana, dos hermanos elfos adolescentes, Ian y Barley Lightfood, se embarcan en una aventura en la que se proponen descubrir si existe aún algo de magia en el mundo que les permita pasar un último día con su padre, que falleció cuando ellos eran aún muy pequeños como para poder recordarlo.',
        director: 'Dan Scanlon',
        type: 'movie',
        original: false,
        year: 2020,
        runtime: '1 h 42 min',
        img: 'https://allgamersin.com/wp-content/uploads/2020/08/Onward_art.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/I8JTARGhwtY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'increibles2',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/48E7CFF93C78D7261E3271E304522E5C64FC1B277AEDD94DCB0E14786A3B11CE/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Familiar, Animación, Superhéroe, Acción y aventura.',
        sinopsis: 'Helen tiene que liderar una campaña para que los superhéroes regresen, mientras Bob vive su vida "normal" con Violet, Dash y el bebé Jack-Jack —cuyos superpoderes descubriremos—. Su misión se va a pique cuando aparece un nuevo villano con un brillante plan que lo amenaza todo. Pero los Parr no se amedrentarán y menos teniendo a Frozone de su parte.',
        director: 'Brad Bird',
        type: 'movie',
        original: false,
        year: 2019,
        runtime: '1 h 32 min',
        img: 'https://blogdesuperheroes.es/wp-content/plugins/BdSGallery/BdSGaleria/68197.jpg,',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/92iqVqYGpxo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'soul',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/AEEE52D4E769B0A2D096A97B9EB3D412C06C09E1ECD2142BA77F398DDD6BBCB1/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Familiar, Comedia, Fantásticas, Animación, Musica',
        sinopsis: '¿Alguna vez te has preguntado de dónde provienen tu pasión, tus sueños y tus intereses? ¿Qué es lo que te hace ser... tú? Pixar te lleva en un viaje desde las calles de Nueva York a los reinos cósmicos para descubrir las respuestas a las preguntas más importantes de la vida.',
        director: 'Pete Docter, Kemp Powers',
        type: 'movie',
        original: true,
        year: 2020,
        runtime: '1 h 16 min',
        img: 'https://s3.amazonaws.com/prod.assets.thebanner/styles/article-large/s3/article/large/MM-513%20Soul%20.jpg?itok=frsLEaYP',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/NloXY1WRSQw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'toystory4',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/1FF7115C2EB1CA1EB4DFA05BC20E7512DA7009C9BF8B8FDF353CFB785D7C6175/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Familiar, Comedia, Animación, Acción y aventura.',
        sinopsis: 'Woody siempre ha tenido claro cuál es su labor en el mundo y su prioridad: cuidar a su dueño, ya sea Andy o Bonnie. Pero cuando Bonnie añade a Forky, un nuevo juguete de fabricación propia, a su habitación, arranca una nueva aventura que servirá para que los viejos y nuevos amigos le enseñen a Woody lo grande que puede ser el mundo para un juguete.',
        director: 'Josh Cooley',
        type: 'movie',
        original: false,
        year: 2020,
        runtime: '1 h 26 min',
        img: 'https://www.filmesrome.com/wp-content/uploads/2019/07/Toy-Story-4-banner.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/hnl5hQaMpks" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'wandavision',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/1C8C59D54C2673E0C856563F180A25DE7C15EB4C1284B3CB4B89744944FA7FB4/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Romance, Misterio, Drama, Ciencia ficción, Superhéroes.',
        sinopsis: 'Combinando el estilo clásico de las sitcoms con el MCU (Universo Cinematográfico de Marvel), cuenta la historia Wanda Maximoff y Vision, dos seres con superpoderes que viven una vida idílica en las afueras de una ciudad hasta que un día comienzan a sospechar que no todo es lo que parece.',
        director: 'Jac Schaeffer, Matt Shakman',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://www.vengadoresmarvel.com/wp-content/uploads/2021/02/Wandavision-Banner-parte-2a-de-la-serie.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/tCE4CpxlaqE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'loki',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/1DE4EB87970708BEF8CA80E8F0326AEBC9A6019B7E974C1C7A181C12A3D4CA9A/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Superhéroes, Fantásticas, Acción y aventuras',
        sinopsis: 'Loki es llevado ante la misteriosa organización llamada Autoridad de Variación Temporal después de robar el Cubo Cósmico durante los eventos de Avengers: Endgame (2019) y se le da a elegir enfrentarse a la eliminación de la realidad o ayudar contra una amenaza mayor,​ por lo cual Loki termina viajando a través del tiempo, alterando la historia humana.',
        director: 'Michael Waldron',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://sm.ign.com/ign_es/news/n/new-loki-p/new-loki-poster-shows-off-the-series-characters-including-a_dwaf.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/uz1CREWyISo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'whatif',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/124252E5116CEEB57EC0A32D5C5A370FD3D53582664AA013F27E56DC579FDD49/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Antalogía, Animación, Superhéroe, Acción y aventura.',
        sinopsis: 'Explora momentos clave del universo Marvel y plantea situaciones alternativas sobre algunos de los personajes más importantes de esas películas.',
        director: 'Bryan Andrews',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://i0.wp.com/brickshow.com/wp-content/uploads/2021/07/banner-2-marvel-what-if.jpg?fit=800%2C480&ssl=1',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/Q4og7q_BWUU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'blackwidow',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/818746A155BE3BD28DB9CAF4A0DFA14E3AABD20EBB1E167D7AF594295FDB95D0/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Espionaje, Superhéroes, Acción y aventura.',
        sinopsis: 'Natasha Romanoff, alias Viuda Negra, se enfrenta a los capítulos más oscuros de su historia cuando surge una peligrosa conspiración relacionada con su pasado. Perseguida por una fuerza que no se detendrá ante nada para acabar con ella, Natasha debe lidiar con su historia como espía y con la estela de relaciones destruidas que dejó atrás mucho antes de convertirse en Vengadora.',
        director: 'Cate Shortland',
        type: 'movie',
        original: false,
        year: 2021,
        runtime: '2h 15 min',
        img: 'https://www.tekcrispy.com/wp-content/uploads/2021/07/black-widow.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/2zHzRBDlWxk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'endgame',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/122E4DD279C944EC6930269FC1A7E8C5838EA2EB34B2E4281D4E93C321BD83A9/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Fantásticas, Superhéroes, Acción y aventura.',
        sinopsis: 'Después de los eventos devastadores de "Avengers: Infinity War", el universo está en ruinas debido a las acciones de Thanos, el Titán Loco. Con la ayuda de los aliados que quedaron, los Vengadores deberán reunirse una vez más para intentar deshacer sus acciones y restaurar el orden en el universo de una vez por todas, sin importar cuáles son las consecuencias... ',
        director: 'Joe Russo, Anthony Russo',
        type: 'movie',
        original: false,
        year: 2019,
        runtime: '3 h 4 min',
        img: 'https://depor.com/resizer/fWxKin2Ay5W1HVK_-RkKIvCya0k=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/4KYYDSLJ2NGCXGHGL533LKFANI.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/UQ3bqYKnyhM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'infinitywar',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/AD9DFFF94EAD6E2B06D2B8D9A59C7DB58091871F25664991C7E7838B686623C4/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Fantásticas, Superhéroes, Acción y aventura.',
        sinopsis: 'El todopoderoso Thanos ha despertado con la promesa de arrasar con todo a su paso, portando el Guantelete del Infinito, que le confiere un poder incalculable. Los únicos capaces de pararle los pies son los Vengadores y el resto de superhéroes de la galaxia, que deberán estar dispuestos a sacrificarlo todo por un bien mayor. Capitán América e Ironman deberán limar sus diferencias, Black Panther apoyará con sus tropas desde Wakanda, Thor y los Guardianes de la Galaxia e incluso Spider-Man se unirán antes de que los planes de devastación y ruina pongan fin al universo. ¿Serán capaces de frenar el avance del titán del caos?',
        director: 'Joe Russo, Anthony Russo',
        type: 'movie',
        original: false,
        year: 2018,
        runtimme: '2 h 32 min',
        img: 'https://fotografias.antena3.com/clipping/cmsimages01/2018/06/13/2AA767EF-798D-4BDB-9B7B-9D1AFF1A9C1B/98.jpg?crop=1036,583,x23,y0&width=1900&height=1069&optimize=high&format=webply',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/-f5PwE_Q8Fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'mandalorian',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/0EF801B36CFCE01C24F9CDDDB0E34A9482956444BCC45DD2933E7705D3326868/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Acción y aventura',
        sinopsis: 'Ambientada tras la caída del Imperio y antes de la aparición de la Primera Orden, la serie sigue los pasos de Mando, un cazarrecompensas perteneciente a la legendaria tribu de los mandalorianos, un pistolero solitario que trabaja en los confines de la galaxia, donde no alcanza la autoridad de la Nueva República.',
        director: 'Jon Favreau',
        type: 'serie',
        original: true,
        year: '2019-2020',
        runtime: '2 temporadas',
        img: 'https://pbs.twimg.com/media/EHRv5d_UUAEMUEk.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/ItIlFvLBfLw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'visions',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/D487B044DF38382F63C28EB58DC7F56DA4D0A49CD199C5E39F5E3CA1F3F1CA07/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Antalogía, Animación, Anime, Acción y aventura.',
        sinopsis: 'Serie de animación que consta de varios episodios independientes animados por múltiples estudios japoneses, ofreciendo visiones ricas y novedosas del universo Star Wars con una filosofía similar a la de proyectos como "Animatrix".',
        director: 'Hiroyuki Imaishi, Masahiko Otsuka, Eunyoung Choi, Abel Góngora, Taku Kimura, Takanobu Mizuno, Kenji Kamiyama, Yuuki Igarashi, Hitoshi Haga',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://lumiere-a.akamaihd.net/v1/images/star-wars-visions-is-here-hero-mobile_17f9914e.jpeg?region=0,0,1024,626&width=960',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/_jtsF_PyR5s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'newhope',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/07C8EF25BC56E120B368682BF26C432D21CA888BA6C44361296E6FC2D2AA6D81/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Acción y aventura.',
        sinopsis: 'La princesa Leia, líder del movimiento rebelde que desea reinstaurar la República en la galaxia en los tiempos ominosos del Imperio, es capturada por las Fuerzas Imperiales, capitaneadas por el implacable Darth Vader, el sirviente más fiel del Emperador. El intrépido y joven Luke Skywalker, ayudado por Han Solo, capitán de la nave espacial "El Halcón Milenario", y los androides, R2D2 y C3PO, serán los encargados de luchar contra el enemigo e intentar rescatar a la princesa para volver a instaurar la justicia en el seno de la galaxia.',
        director: 'George Lucas',
        type: 'movie',
        original: false,
        year: 1977,
        runtime: '2 h 5 min',
        img: 'https://www.cinemascomics.com/wp-content/uploads/2019/11/poster-star-wars-una-nueva-esperanza.jpg?width=1200&enable=upscale',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/beAH5vea99k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'empire',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/286E45999F72C6E1725217E741532FC514D110B75A464D2E484BAEEA71586C80/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Acción y aventura.',
        sinopsis: 'Tras un ataque sorpresa de las tropas imperiales a las bases camufladas de la alianza rebelde, Luke Skywalker, en compañía de R2D2, parte hacia el planeta Dagobah en busca de Yoda, el último maestro Jedi, para que le enseñe los secretos de la Fuerza. Mientras, Han Solo, la princesa Leia, Chewbacca, y C3PO esquivan a las fuerzas imperiales y piden refugio al antiguo propietario del Halcón Milenario, Lando Calrissian, en la ciudad minera de Bespin, donde les prepara una trampa urdida por Darth Vader.pública en la galaxia en los tiempos ominosos del Imperio, es capturada por las Fuerzas Imperiales, capitaneadas por el implacable Darth Vader, el sirviente más fiel del Emperador. El intrépido y joven Luke Skywalker, ayudado por Han Solo, capitán de la nave espacial "El Halcón Milenario", y los androides, R2D2 y C3PO, serán los encargados de luchar contra el enemigo e intentar rescatar a la princesa para volver a instaurar la justicia en el seno de la galaxia.',
        director: 'George Lucas',
        type: 'movie',
        original: false,
        year: 1977,
        runtime: '2 h 7 min',
        img: 'https://images7.alphacoders.com/111/thumb-350-1115522.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/xr3hPFJAHO4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'jedi',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/286E45999F72C6E1725217E741532FC514D110B75A464D2E484BAEEA71586C80/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Acción y aventura.',
        sinopsis: 'Para ir a Tatooine y liberar a Han Solo, Luke Skywalker y la princesa Leia deben infiltrarse en la peligrosa guarida de Jabba the Hutt, el gángster más temido de la galaxia. Una vez reunidos, el equipo recluta a tribus de Ewoks para combatir a las fuerzas imperiales en los bosques de la luna de Endor. Mientras tanto, el Emperador y Darth Vader conspiran para atraer a Luke al lado oscuro, pero el joven está decidido a reavivar el espíritu del Jedi en su padre. La guerra civil galáctica termina con un último enfrentamiento entre las fuerzas rebeldes unificadas y una segunda Estrella de la Muerte, indefensa e incompleta, en una batalla que decidirá el destino de la galaxia.',
        director: 'George Lucas',
        type: 'movie',
        original: false,
        year: 1983,
        runtime: '2 h 15 min',
        img: 'https://www.lafinestradigital.com/wp-content/uploads/2011/09/starwars-episodeVI.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/yhuKapE-Bio" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: "jeffgoldblum",
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/6DF2634D81177B353ACE8ACBDBDC1BDEE3B54FF66CB99D8E64F4A8C22832F000/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Docuserie',
        sinopsis: 'En cada uno de estos 12 episodios, Jeff Goldblum tira del hilo de un objeto sorprendentemente familiar para descubrir un mundo de conexiones sorprendentes y de ciencia e historia fascinantes. Visto con su curiosidad e inteligencia, nada es lo que parece. Estas "maravillas modernas" son tan comunes que a veces damos por sentada su existencia... pero Jeff Goldblum no.',
        director: 'Karen McGann, Nic Stacey, Simon Lloyd Roberts',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://conlagentenoticias.com/wp-content/uploads/2020/11/jeff-goldblum.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/_ocVsvg-jKE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'bienvenidostierra',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/CAC7FE404E751BDDE6C7D3AE57B8A55AE4A4B7E3F2D00328CB7AC010A42D65B3/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Familiar, Docuserie, Animales y naturaleza.',
        sinopsis: 'Desde volcanes en activo hasta las profundidades del océano, Will Smith viaja a los confines de la Tierra, emprendiendo una aventura alrededor del mundo para explorar las grandes maravillas de nuestro planeta.',
        director: 'Darren Aronofsky, Rod Blackhurst',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/0E4812770E5D8BF71C43EDBE23A637717E71FBDA62E137956775A04A9EDA2E60/scale?width=1200&aspectRatio=1.78&format=jpeg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/ueNviXyC4x0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'sinlimites',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/73C2CFC3CF6C1DC3938919CFAED6A1F7FEE17E06F9000A13EE1CC016E2B1DB0A/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Docuserie, Estilo de vida',
        sinopsis: 'Una forma diferente en la que podemos vivir mejor durante más tiempo: regenerando el daño, maximizando la fuerza, construyendo resiliencia, impactando al cuerpo, sobrealimentando la memoria y enfrentando la mortalidad.',
        director: 'Kit Lynch Robinson',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://flixable.b-cdn.net/disney-plus/large/es/limitless-with-chris-hemsworth.png',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/dlkLXr-aFRo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'jugandotiburones',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/E0DE160764849BEC97C68DBD2855F2C21BD5FF07D0954E55CE1F1536A879F62B/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Documental, Biografía, Animales y naturaleza',
        sinopsis: 'Valerie Taylor, buceadora pionera , ha dedicado su vida a exponer el mito que rodea nuestro miedo a los tiburones.',
        director: 'Sally Aitken',
        type: 'serie',
        original: true,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://flixable.b-cdn.net/disney-plus/small/es/playing-with-sharks.png',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/X4ZZJJPn-rM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'emprendedores',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/354AA8535C4DDC0344FA899C661725A354C9D1AB7FAD6F64CB6052250846ECA1/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Documental',
        sinopsis: 'Cinco jóvenes emprendedores, de diferentes partes del planeta, se enfrentan en una de las competiciones empresariales más prestigiosas del mundo.',
        director: 'Cristina Costanini, Darren Foster',
        type: 'movie',
        original: true,
        year: 2021,
        runtime: '1 h 32 min',
        img: 'https://flixable.b-cdn.net/disney-plus/large/es/own-the-room.png',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/ix8f5gOl2rI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'hitmonkey',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/CFC9FA1873C3CA5F7438AF928CF8A43697532CC1828AA17A02E5DA8A03CA9C2A/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Comedia, Animación, Policiaca, Acción y aventura.',
        sinopsis: 'Un mono japonés, que es un experto tirador y tiene una gran agilidad y reflejos, se entrena para hacer un viaje de venganza oscuro y sangriento por los lugares más peligrosos de Tokyo. Para este entrenamiento cuenta con la gran ayuda del fantasma de un asesino americano.',
        director: 'Josh Gordon, Will Speck, Neal Holman',
        type: 'serie',
        original: false,
        year: 2021,
        runtime: '1 temporada',
        img: 'https://media.revistagq.com/photos/61924b5df654c19d66ebdc9b/16:9/w_1280,c_limit/monkey.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/9AcQxOYcTGg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'simpson',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/E5B732D6A1D19A7BD6792853AA75C8E7DC25F3DD32ED60E3615BFDF8C0682459/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Comedia, Animación.',
        sinopsis: 'Narra la historia de una peculiar familia (Homer, Marge, Bart, Maggie y Lisa Simpson) y otros divertidos personajes de la localidad norteamericana de Springfield. Homer, el padre, es un desastroso inspector de seguridad de una central nuclear. Marge, la madre, es un ama de casa acostumbrada a soportar a su peculiar familia. Bart, de 10 años, intenta divertirse con travesuras de todo tipo. Lisa es la más inteligente de la familia, y Maggie, la más pequeña, es un bebé que todavía no habla, pero que juega un importante papel.',
        direcotr: 'Matt Groening',
        type: 'serie',
        original: false,
        year: '1989-2022',
        runtime: '32 temporadas.',
        img: 'https://i0.wp.com/codigoespagueti.com/wp-content/uploads/2020/10/Los-Simpson.jpg?fit=1280%2C720&quality=80&ssl=1',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/E3ZoIPyHRMA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'alien',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/FC49F42518464757E829D3AB763886D78288B15B536E8C317A984C0AA3E67C9F/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Terror, Ciencia ficción, Thriller.',
        sinopsis: 'De regreso a la Tierra, la nave de carga Nostromo interrumpe su viaje y despierta a sus siete tripulantes. El ordenador central, MADRE, ha detectado la misteriosa transmisión de una forma de vida desconocida, procedente de un planeta cercano aparentemente deshabitado. La nave se dirige entonces al extraño planeta para investigar el origen de la comunicación.',
        director: 'Ridley Scott',
        type: 'movie',
        original: false,
        year: 1979,
        runtime: '1 h 57 min',
        img: 'https://frikerio.files.wordpress.com/2018/10/img_20181016_004230.jpg?w=800',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/Eu9ZFTXXEiw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'prometheus',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/7660964FC4A23EB70C60563C38ABC4F30CA7AA3C59CEB59FD08E27A510FDED68/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Ciencia ficción, Acción y aventura.',
        sinopsis: 'Finales del siglo XXI. Un grupo de científicos y exploradores emprende un viaje espacial de más de dos años en la nave Prometheus a un remoto planeta recién descubierto, donde su capacidad física y mental será puesta a prueba. El objetivo de la misión es encontrar respuesta al mayor de los misterios: el origen de la vida en la Tierra.',
        director: 'Ridley Scott',
        type: 'movie',
        original: false,
        year: 2012,
        runtime: '2 h 24 min',
        img: 'https://1.bp.blogspot.com/-Yy6ISIzAZ6I/UMDRToIFlZI/AAAAAAAAP0k/4PLDXPa5vLE/s1600/Prometheus+banner.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/4VV4PTk9jMI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'moulinrouge',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/8C833239841C580A7F8BF18E39F3433E14BB42BB137AD7ACAF50A3A9FC015F58/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'Romance, Musical',
        sinopsis: 'Ambientada en el París bohemio de 1900. Satine, la estrella más rutilante del Moulin Rouge, encandila a toda la ciudad con sus bailes llenos de sensualidad y su enorme belleza. Atrapada entre el amor de dos hombres, un joven escritor y un duque, lucha por hacer realidad su sueño de convertirse en actriz. Pero, en un mundo en el que todo vale, excepto enamorarse, nada es fácil.',
        director: 'Baz Luhrmann',
        tyoe: 'movie',
        original: false,
        year: 2001,
        runtime: '2 h 8 min',
        img: 'https://i.pinimg.com/originals/9d/89/fb/9d89fba10d22d564feb6b6774ee3209b.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/LVLjp3_MQIw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    },

    {
        name: 'luca',
        title: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/3DBEF3579BA43A96D8E2D28AB37530B56DF9A1DBB3C7B23D86E1B614D53D8396/scale?width=1440&aspectRatio=1.78&format=png',
        category: 'De adolescente a adulto, Familiar, Fantásticas, Animación, Amistad.',
        sinopsis: 'En un hermoso pueblo en la Riviera italiana, Luca y Alberto disfrutan del verano mientras intentan ocultar su gran secreto: ambos son monstruos marinos que se convierten en humanos cuando están secos.',
        director: 'Enrico Casarosa',
        type: 'movie',
        original: true,
        year: 2021,
        runtime: '1 h 41 min',
        img: 'https://cartelescine.files.wordpress.com/2021/02/lucabanner.jpg',
        trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/zt1exa_FMSA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
    }

    
]


export default DB