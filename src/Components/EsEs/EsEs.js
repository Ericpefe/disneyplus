import React from "react";
import {useNavigate} from 'react-router-dom'
import "./EsEs.css"

const EsEs = () => {
  const navigate = useNavigate()
      
  
     return (
       <div className="Home">
      <button onClick={()=>navigate('login')} className='log'>INICIA SESIÓN</button>
       <div>
       <img className="logo" alt="disney+ logo" src="https://cnbl-cdn.bamgrid.com/assets/7ecc8bcb60ad77193058d63e321bd21cbac2fc67281dbd9927676ea4a4c83594/original"></img>
       <h1>Las mejores historias, en un mismo lugar.</h1>
       <div className="subscribe">
         <div className="links-sub">
       <h2>8,99 | Mes</h2>
       <button onClick={()=>navigate('subscribe')} className="sub-content">SUSCRÍBETE YA</button>
       </div>
       <div className="links-sub">
       <h3>89.90 | Año</h3>
       <p>Ahorra dos meses con la subscripción anual*</p>
       <button onClick={()=>navigate('subscribe')} className="sub-content">SUSCRÍBETE Y AHORRA</button>
       </div>
       </div>
       <p>Aplican términos y condiciones. *Ahorro aplicable a suscripciones anuales en comparación con el precio de doce meses a precio de suscripción mensual.</p>
       </div>
       <div className="content">
         <div className="content-pic">
         <img className="wandavision" alt="Wandavision" src="https://cnbl-cdn.bamgrid.com/assets/b36d3b14f17535b984fdfa470031e2b143e210a8bac8274b37b161e774505b6d/original"></img>
         </div>
         <div className="content-fit">
           <h1>Disfruta de Disney+ en compañía</h1>
           <ul>
             <li>Noches de series y películas con GroupWatch. Para invitar o que te inviten a GroupWatch, es necesario tener una suscripción.</li>
             <li>Disfruta de Disney+ en compañía, aunque estéis en diferentes lugares.</li>
             <li>Visualización simultánea con hasta 6 personas.</li>
             <li>Pausad, rebobinad y reaccionad juntos</li>
             <li>Fácil de configurar y compartir.</li>
           </ul>
         </div>
         <button onClick={()=>navigate('subscribe')} className="sub-content">SUSCRÍBETE YA</button>
         <div className="available">
           <h1>Disponible en tus dispositivos favoritos</h1>
           <div className="dispositivos">
             <ul>
               <img className="dispositive" src="https://cnbl-cdn.bamgrid.com/assets/00fb59319fa715222100d8a84d11bc7e23a42970b4f413c9e85166d0cfba9346/original" alt="tv"></img>
               <p className="title">Tv</p>
               <li>Amazon Fire TV</li>
               <li>Dispositivos Android TV</li>
               <li>Apple TV</li>
               <li>Chromecast</li>
               <li>Televisores LG</li>
               <li>Roku</li>
               <li>Samsung</li>
             </ul>
  
             <ul>
               <img className="dispositive" src="https://cnbl-cdn.bamgrid.com/assets/d73b7c534afd2af2a454dbd47bd6c766c70e334ce8137084e9cd25c2644dd267/original" alt="computer"></img>
               <p className="title">Ordenadores</p>
               <li>Chrome OS</li>
               <li>MacOS</li>
               <li>Windows PC</li>
             </ul>
           
             <ul>
               <img className="dispositive" src="https://cnbl-cdn.bamgrid.com/assets/66475056e769443ef9a491a48dfa44059c8964890ae9ef7c4f69f322693c59d8/original" alt="phone"></img>
               <p className="title">Móvil y Tablet</p>
               <li>Tablets Amazon Fire</li>
               <li>Móviles y Tablets Android</li>
               <li>iPhone and iPad</li>
             </ul>
           
             <ul>
               <img className="dispositive" src="https://cnbl-cdn.bamgrid.com/assets/51b639d2ebe97ee175975c29d42a90b0e043713856db8e5d6d9fb87b2b3a48c0/original" alt="console"></img>
               <p className="title">Videoconsolas</p>
               <li>PS4</li>
               <li>PS5</li>
               <li>Xbox One</li>
               <li>Xbox Series X</li>
               <li>Xbox Series S</li>
             </ul>
           </div>
         </div>
       </div>
       </div>
     )
}

export default EsEs