import React, { useState } from "react";
import {useNavigate} from 'react-router-dom'


const Login = (props) => {

    
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");


  const navigate = useNavigate()
 
  const handleEmail = (e) => {
    let data = e.target.value
    setEmail(data)
  }

  const handlePass = (e) => {
    let data = e.target.value
    setPassword(data)
  }

  const handleSubmit = (e) => { 
 e.preventDefault()
   let em = localStorage.getItem('email')
   let pass = localStorage.getItem('password')
   let name = localStorage.getItem('username')
  if (em === email && pass === password){
    props.setUser(true)
    alert(`Bienvenido ${name}.`) 
   navigate("/home")
  }else {
      alert('Porfavor, introduce un email/contraseña correctos.')
  }
      
    }

 
     return(
         <div className="Login">
         <img className="logo" src="https://cnbl-cdn.bamgrid.com/assets/7ecc8bcb60ad77193058d63e321bd21cbac2fc67281dbd9927676ea4a4c83594/original" alt="logo"></img>
         <p>Inicia sesión con tu correo electroico</p>
         <form onSubmit={handleSubmit} className="inputs">
             <input onChange={handleEmail} name="email" placeholder="Introduce tu email" type={"email"} />
             <input  onChange={handlePass} name="password" placeholder="Introduce tu contraseña" type={"password"}/>
             <button>Continuar</button>
         </form>
         <div className="footer">
         <p>Primera vez en dinsey+?</p>
         <button onClick={()=>navigate('/subscribe')} style={{color: 'darkblue', textDecoration: 'none'}}>Suscribete</button>
         </div>
         </div>
     )
}


export default Login