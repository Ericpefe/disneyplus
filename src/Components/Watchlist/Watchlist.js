import React from "react";
import { useNavigate } from "react-router-dom";
import Navbar from "../Home/Navbar";
import mylist from "../mylist";


const Watchlist = (props) => {

    const navigate = useNavigate()
    
    return (
        <div>
            <Navbar setUser={props.setUser} />
            <div>
            <h1>Mi lista</h1>
           <div className="objList">
            {mylist.map((object, idx)=>{
            return(
                <img
                key={idx}
                onClick={()=> navigate(`/home/${object.name}`)}
                 className="newImg"
                src={object.img}
                alt="new releases"
                />
            )
        })}
        </div>
            </div>
        </div>
    )
}


export default Watchlist
