import React from "react";
import { useParams } from "react-router-dom";
import Navbar from "../Home/Navbar";
import mylist from "../mylist";
import "./Display.css";


const Display = (props) => {
   const DB = props.DB;
   let params = useParams();
   let obj;


   


   const add = (obj) => {
       console.log(obj.name)
       console.log(mylist)
         if (mylist.includes(obj)){
             alert(`Ya esta añadida en la lista.`)
         }else{
             mylist.push(obj)
             console.log(mylist)
         }
          }
     



   const watchObj = (object, idx) => {
       if (params.name === object.name){
           object.idx = idx
          obj = object
          
       }
    }
    
    const render = () => {
       if (typeof(obj) === 'object'){          
           return (
               <div key={obj.idx}>
                   <img className={'title'} alt={obj.name} src={obj.title}></img>
                   <div className="info">
                   <p>{obj.year}</p>
                   <p>{obj.runtime}</p>
                   <p>{obj.director}</p>
                   </div>
                   <div className="sinopsis">
                       <div className="sinopsisText">
                   <p>{obj.category}</p>
                   <p style={{width: "90%"}}>{obj.sinopsis}</p>
                   </div>
                   {obj.trailer}
                   </div>
                   <button className="buttonDisplay" onClick={()=>add(obj)}>+</button>
               </div>
           )
        

       }else {
           return (
               <div>
                   <h1>Lo sentimos, tu búsqueda no tiene resultados</h1>
               </div>
           )
       }
   }

   return (
       <div>
           <Navbar />
           {DB.map((obj, idx)=>{
                    return watchObj(obj, idx)
           })}
           {render()}
       </div>
   )
}


export default Display