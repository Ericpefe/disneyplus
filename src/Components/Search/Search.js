import React, { useState } from "react";
import DB from "../DB";
import { useNavigate } from "react-router-dom";
import Navbar from "../Home/Navbar";


const Search = (props) => {

    const [display, setDisplay] = useState(false)
    const [object, setObject] = useState()
    const [name, setName] = useState("")


    const navigate = useNavigate()


    const handleName = (e) => {
        let data = e.target.value
        setName(data)
    }

    const check = (obj) => {
        const nameObj = obj.name
        
        if (nameObj.includes(name) === true){
            setDisplay(true)
            setObject(obj)
         }
    }

    const submit = (e) => {
        e.preventDefault()
      DB.map((obj) => {
          return check(obj)
      })
    }

    const movie = () => {
        if (display){
            return (
                <img
                onClick={()=> navigate(`/home/${object.name}`)}
                 className="newImg"
                src={object.img}
                alt="new releases"
                />
            )
        }
    }
    
  return (
      <div>
          <Navbar setUser={props.setUser} />
      <div style={{marginLeft: "15%"}}>
          <form onSubmit={submit}>
              <input style={{width: '80%'}} onChange={handleName} type={"text"}></input>
              <button className="inputButton">Buscar</button>
          </form>
          {movie()}
      </div>
      </div>
  )
}


export default Search