import React, { useState } from "react";
import {useNavigate} from 'react-router-dom'
import "./Sub.css"


const Sub = () => {

    const [email, setEmail] = useState("")
    const [pass1, setPass1] = useState("")
    const [pass2, setPass2] = useState("")
    const [name, setName] = useState("")


    const navigate = useNavigate()
   

    const handleEmail = (e) => {
        let data = e.target.value
        setEmail(data)
      }
    
      const handlePass1 = (e) => {
        let data = e.target.value
        setPass1(data)
      }
    
      const handlePass2 = (e) => {
        let data = e.target.value
        setPass2(data)
      }

      const handleName = (e) => {
        let data = e.target.value
        setName(data)
      }
    
    
      const handleSubmit = (e) => {
          e.preventDefault()
          if (pass1 === pass2 && pass1.length >= 8  && name !== ''){
            localStorage.setItem('email', email);
            localStorage.setItem('password', pass1);  
            localStorage.setItem('username', name)      
           navigate('/login')
          }else if(pass1 !== pass2) {
              alert('Introduce dos contraseñas iguales')
          }else if (name === ''){
             alert('Introduce un nombre de usuario')
          }else {
            alert('Introduce una contraseña de 8 caracteres o más.')
          }
         
    
      }


   return (
       <div className="sub">
            <img  className="logo" src="https://cnbl-cdn.bamgrid.com/assets/7ecc8bcb60ad77193058d63e321bd21cbac2fc67281dbd9927676ea4a4c83594/original" alt="logo"></img>
           <p>Suscribete useando tu correo electronico</p>
           <form className="subForm" onSubmit={handleSubmit}>
               <input onChange={handleEmail} name="email" placeholder="Introduce tu email" type={"email"} />
               <input onChange={handleName} name="username" placeholder="Introduce un nombre de usuario" type={"text"} />
               <input onChange={handlePass1} name="password" placeholder="Introduce una contraseña de al menos 8 caracteres" type={"password"} />
               <input onChange={handlePass2} placeholder="Repite la contraseña" type={"password"}/>
               <button>Subscribirse</button>
           </form>
       </div>
   )
}


export default Sub