import React from "react";
import SlideshowBanner from "./Slideshows/SlideshowBanner";
import NewDisney from "./categoriesMenu/NewDisney";
import Navbar from "./Navbar";
import Disney from "./categoriesMenu/Disney";
import Pixar from "./categoriesMenu/Pixar";
import Marvel from "./categoriesMenu/Marvel";
import Wars from "./categoriesMenu/Wars";
import National from "./categoriesMenu/National";
import Star from "./categoriesMenu/Star";
import "./Home.css";



const Home = (props) => {


  return (
      <div className="menu">
        <Navbar setUser={props.setUser} />
        <div className="main">
           <SlideshowBanner />
           <div className="buttonNav">
             <a href="#disney"><img className="buttonMain" alt="Disney" src="https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/FFA0BEBAC1406D88929497501C84019EBBA1B018D3F7C4C3C829F1810A24AD6E/scale?width=400&aspectRatio=1.78&format=png"></img></a>
             <a href="#pixar"><img className="buttonMain" src='https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/7F4E1A299763030A0A8527227AD2812C049CE3E02822F7EDEFCFA1CFB703DDA5/scale?width=400&aspectRatio=1.78&format=png' alt='Pixar'></img></a>
             <a href="#marvel"><img className="buttonMain" alt="Marvel" src="https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/C90088DCAB7EA558159C0A79E4839D46B5302B5521BAB1F76D2E807D9E2C6D9A/scale?width=400&aspectRatio=1.78&format=png"></img></a>
             <a href="#wars"><img className="buttonMain" alt="Star Wars" src="https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/5A9416D67DC9595496B2666087596EE64DE379272051BB854157C0D938BE2C26/scale?width=400&aspectRatio=1.78&format=png"></img></a>
             <a href="#national"><img className="buttonMain" alt="National" src="https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/2EF24AA0A1E648E6D1A3B26491F516632137ED87AB22969D153316F8BD670FB5/scale?width=400&aspectRatio=1.78&format=png"></img></a>
             <a href="#star"><img className="buttonMain" alt="Star" src="https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/AE893BCDD6264C4A876C03A0DE5004D9F394BE1E8388F085431318CDCEC9A598/scale?width=400&aspectRatio=1.78&format=png"></img></a>
           </div>
           <div className="cateogryMain">
             <NewDisney />
             <Disney />
             <Pixar />
             <Marvel />
             <Wars />
             <National />
             <Star />
           </div>
        </div>
      </div>
  )
}


export default Home;