import React from "react";
import Navbar from "./Navbar";
import DB from "../DB";
import { useNavigate } from "react-router-dom";


const Serie = (props) => {
    const navigate = useNavigate()

    const displayObj = (object, idx) => {
        if(object.type === 'serie'){
            return (
                <img
                key={idx}
                onClick={()=> navigate(`/home/${object.name}`)}
                 className="objImg"
                src={object.img}
                alt="original"
                />
            )
        }
    }

    return(
        <div>
            <Navbar setUser={props.setUser} />
            <div>
                <h1>SERIES</h1>
                <div className="objList">
            {DB.map((obj, idx)=>{
                return displayObj(obj, idx)
            })}
            </div>
            </div>
        </div>
    )

}


export default Serie