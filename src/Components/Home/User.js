import React, { useState } from "react"
import { useNavigate } from "react-router-dom"
import "./User.css"


const User = (props) => {

    const [pStyle, setPstyle] = useState('noRender')
    const navigate = useNavigate()

    

    let name = localStorage.getItem('username')

    const logOut = () => {
         props.setUser(false)
         alert(`Adios ${name}.`)
         navigate('/')
    }

    const changeStyle = () => {
        if (pStyle === 'noRender'){
            setPstyle('render')
        }else {
            setPstyle('noRender')     
        }
    }
   

    return (
        <div className="user">
            <p onClick={()=>changeStyle()}>{name}</p>
        <p className={pStyle} onClick={()=>logOut()}>Cerrar sesión</p>
        </div>
    )
    
}


export default User

