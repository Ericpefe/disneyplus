import React from "react";
import { useNavigate } from "react-router-dom";
import "./categories.css";


const NewDisney = () => {


  const navigate = useNavigate()

  

  let newSrc = [
    {
    name: "encanto",
    link: "http://t1.gstatic.com/images?q=tbn:ANd9GcSHskZ06fUYhjWlD6DfQgFGNn-yDeIlbIOy22557QyDMEEi2Ta7"
  },
  {
    name: "greyanatomy",
    link: "https://static.klisst.com/4338141a-c6c8-4eca-aab7-866abb2d9984.jpeg"
  },
  {
    name: "dugcarl",
    link: "https://ocioworld.files.wordpress.com/2021/09/scale.jpg"
  },
  {
    name: "eternals",
    link: "https://sm.ign.com/ign_latam/screenshot/default/enelprincipio_r2w1.jpg"
  },
  {
    name: "librobobafett",
    link: "https://www.starwarsnewsnet.com/wp-content/uploads/2021/11/FEAXvmoXEAoIWfE.jpg"
  }
]
    return(
    <div id="newDisney" className="newCat">
               <h1>Nuevo en disney+</h1>
        
            <div className="newList">
              {newSrc.map((obj, index) => (
               
                  <img
                  onClick={()=> navigate(`/home/${obj.name}`)}
                   className="newImg"
                   key={index}
                  src={obj.link}
                  alt="new releases"
                  />
                
            ))}
            </div>
               </div>
    )
}


export default NewDisney