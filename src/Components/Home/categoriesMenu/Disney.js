import React from "react";
import { useNavigate } from "react-router-dom";

const Disney = () => {

    const navigate = useNavigate()


    const newSrc = [
        {
            name: 'cenicienta',
            link: 'https://www.misfiestas.es/wp-content/uploads/2017/05/cinderella-top-banner.jpg'
        },
        {
            name: 'hercules',
            link: 'https://muzikspeaks.files.wordpress.com/2015/07/hercules-banner.jpg'
        },
        {
            name: 'libroselva',
            link: 'http://3.bp.blogspot.com/-YJDd8uemRjE/UYts4L14W2I/AAAAAAAA67o/459YW1K0vs4/s1600/The-Jungle-Book-poster+cartel+high+quality+blu+ray+trailer+dvd+platinum+diamond+edition+walt+disney+1967+2013+el+libro+de+la+selva+mowgli+baloo+bagheera+shere+khan+kaa+rey+louie+banner.png'
        },
        {
            name: 'sirenita',
            link: 'https://www.agendamenuda.es/images/0-que-hacer/2017/cine/la-sirenita.jpg'
        },
        {
            name: 'frozen',
            link: 'https://blogs.eitb.eus/estrenosdecine/wp-content/uploads/sites/31/2014/09/frozen-banner-final1.jpg'
        }
    ]


    return(
        <div id="disney" className="newCat">
                   <h1>Disney</h1>
            
                <div className="newList">
                  {newSrc.map((obj, index) => (
                   
                      <img
                      onClick={()=> navigate(`/home/${obj.name}`)}
                       className="newImg"
                       key={index}
                      src={obj.link}
                      alt="Disney"
                      />
                    
                ))}
                </div>
                   </div>
        )
}


export default Disney