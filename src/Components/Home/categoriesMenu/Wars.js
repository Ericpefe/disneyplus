import React from "react";
import { useNavigate } from "react-router-dom";

const Wars = () => {

    const navigate = useNavigate()


    const newSrc = [
        
          {
            name: "librobobafett",
            link: "https://www.starwarsnewsnet.com/wp-content/uploads/2021/11/FEAXvmoXEAoIWfE.jpg"
          },
          {
              name: 'visions',
              link: 'https://lumiere-a.akamaihd.net/v1/images/star-wars-visions-is-here-hero-mobile_17f9914e.jpeg?region=0,0,1024,626&width=960'
          },
          {
              name: 'newhope',
              link: 'https://www.cinemascomics.com/wp-content/uploads/2019/11/poster-star-wars-una-nueva-esperanza.jpg?width=1200&enable=upscale'
          },
          {
              name: 'empire',
              link: 'https://images7.alphacoders.com/111/thumb-350-1115522.jpg'
          },
          {
              name: 'jedi',
              link: 'https://www.lafinestradigital.com/wp-content/uploads/2011/09/starwars-episodeVI.jpg'
          }
    ]


    return(
        <div id="wars" className="newCat">
                   <h1>Star Wars</h1>
            
                <div className="newList">
                  {newSrc.map((obj, index) => (
                   
                      <img
                      onClick={()=> navigate(`/home/${obj.name}`)}
                       className="newImg"
                       key={index}
                      src={obj.link}
                      alt="Wars"
                      />
                    
                ))}
                </div>
                   </div>
        )
}


export default Wars