import React from "react";
import { useNavigate } from "react-router-dom";

const National = () => {

    const navigate = useNavigate()


    const newSrc = [
          {
            name: "rescate",
            link: "https://i0.wp.com/cuatrobastardos.com/wp-content/uploads/2021/12/The-Rescue-Poster.jpg?resize=1080%2C1350&ssl=1"
          },
          {
              name: 'bienvenidostierra',
              link: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/0E4812770E5D8BF71C43EDBE23A637717E71FBDA62E137956775A04A9EDA2E60/scale?width=1200&aspectRatio=1.78&format=jpeg'
          },
          {
              name: 'sinlimites',
              link: 'https://flixable.b-cdn.net/disney-plus/large/es/limitless-with-chris-hemsworth.png'
          },
          {
              name: 'jugandotiburones',
              link: 'https://flixable.b-cdn.net/disney-plus/small/es/playing-with-sharks.png'
          },
          {
              name: 'emprendedores',
              link: 'https://flixable.b-cdn.net/disney-plus/large/es/own-the-room.png'
          }
    ]


    return(
        <div id="national" className="newCat">
                   <h1>National</h1>
            
                <div className="newList">
                  {newSrc.map((obj, index) => (
                   
                      <img
                      onClick={()=> navigate(`/home/${obj.name}`)}
                       className="newImg"
                       key={index}
                      src={obj.link}
                      alt="National"
                      />
                    
                ))}
                </div>
                   </div>
        )
}


export default National