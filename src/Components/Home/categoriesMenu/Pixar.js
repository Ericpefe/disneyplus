import React from "react";
import { useNavigate } from "react-router-dom";

const Pixar = () => {

    const navigate = useNavigate()


    const newSrc = [
         {
             name: 'delreves',
             link: 'http://4.bp.blogspot.com/-G44luHloOCU/VapgT6XiltI/AAAAAAAAD1g/Fqt4Kwjhf2A/s1600/del_reves_banner_0%2B%2528600%2529.jpg'
         },
         {
             name: 'monstruosobra',
             link: 'https://i0.wp.com/hipertextual.com/wp-content/uploads/2021/06/monstruos_a_la_obra_pete_docter.jpg?fit=1600%2C1067&ssl=1',
         },
         {
             name: 'onward',
             link: 'https://allgamersin.com/wp-content/uploads/2020/08/Onward_art.jpg'
         },
         {
             name: 'increibles2',
             link: 'https://blogdesuperheroes.es/wp-content/plugins/BdSGallery/BdSGaleria/68197.jpg'
         },
         {
             name: 'toystory4',
             link: 'https://www.filmesrome.com/wp-content/uploads/2019/07/Toy-Story-4-banner.jpg'
         }
    ]


    return(
        <div id="pixar" className="newCat">
                   <h1>Pixar</h1>
            
                <div className="newList">
                  {newSrc.map((obj, index) => (
                   
                      <img
                      onClick={()=> navigate(`/home/${obj.name}`)}
                       className="newImg"
                       key={index}
                      src={obj.link}
                      alt="Pixar"
                      />
                    
                ))}
                </div>
                   </div>
        )
}


export default Pixar