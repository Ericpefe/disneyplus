import React from "react";
import { useNavigate } from "react-router-dom";

const Star = () => {

    const navigate = useNavigate()


    const newSrc = [
         {
             name: 'hitmonkey',
             link: 'https://media.revistagq.com/photos/61924b5df654c19d66ebdc9b/16:9/w_1280,c_limit/monkey.jpg'
         },
         {
             name: 'simpson',
             link: 'https://i0.wp.com/codigoespagueti.com/wp-content/uploads/2020/10/Los-Simpson.jpg?fit=1280%2C720&quality=80&ssl=1'
         },
         {
             name: 'alien',
             link: 'https://frikerio.files.wordpress.com/2018/10/img_20181016_004230.jpg?w=800'
         },
         {
             name: 'prometheus',
             link: 'https://1.bp.blogspot.com/-Yy6ISIzAZ6I/UMDRToIFlZI/AAAAAAAAP0k/4PLDXPa5vLE/s1600/Prometheus+banner.jpg'
         },
         {
             name: 'moulinrouge',
             link: 'https://i.pinimg.com/originals/9d/89/fb/9d89fba10d22d564feb6b6774ee3209b.jpg'
         }
    ]


    return(
        <div id="star" className="newCat">
                   <h1>Star</h1>
            
                <div className="newList">
                  {newSrc.map((obj, index) => (
                   
                      <img
                      onClick={()=> navigate(`/home/${obj.name}`)}
                       className="newImg"
                       key={index}
                      src={obj.link}
                      alt="Star"
                      />
                    
                ))}
                </div>
                   </div>
        )
}


export default Star