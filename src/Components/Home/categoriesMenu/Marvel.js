import React from "react";
import { useNavigate } from "react-router-dom";

const Marvel = () => {

    const navigate = useNavigate()


    const newSrc = [
           {
               name: 'wandavision',
               link: 'https://blogdesuperheroes.es/wp-content/plugins/BdSGallery/BdSGaleria/101594.jpg'
           },
           {
               name: 'whatif',
               link: 'https://i0.wp.com/brickshow.com/wp-content/uploads/2021/07/banner-2-marvel-what-if.jpg?fit=800%2C480&ssl=1'
           },
           {
               name: 'blackwidow',
               link: 'https://www.tekcrispy.com/wp-content/uploads/2021/07/black-widow.jpg'
           },
           {
               name: 'endgame',
               link: 'https://depor.com/resizer/fWxKin2Ay5W1HVK_-RkKIvCya0k=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/4KYYDSLJ2NGCXGHGL533LKFANI.jpg'
           },
           {
               name: 'infinitywar',
               link: 'https://fotografias.antena3.com/clipping/cmsimages01/2018/06/13/2AA767EF-798D-4BDB-9B7B-9D1AFF1A9C1B/98.jpg?crop=1036,583,x23,y0&width=1900&height=1069&optimize=high&format=webply'
           }
    ]


    return(
        <div id="marvel" className="newCat">
                   <h1>Marvel</h1>
            
                <div className="newList">
                  {newSrc.map((obj, index) => (
                   
                      <img
                      onClick={()=> navigate(`/home/${obj.name}`)}
                       className="newImg"
                       key={index}
                      src={obj.link}
                      alt="Marvel"
                      />
                    
                ))}
                </div>
                   </div>
        )
}


export default Marvel