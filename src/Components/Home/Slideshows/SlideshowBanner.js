import React, { useState, useRef, useEffect }  from "react";
import { useNavigate } from "react-router-dom";
import './SlideshowsBanner.css'


const slideImages = [
  {
   name: "wandavision",
   link: 'https://www.vengadoresmarvel.com/wp-content/uploads/2021/02/Wandavision-Banner-parte-2a-de-la-serie.jpg'
 }, 
 {
  name: "encanto",
  link: 'https://m.media-amazon.com/images/S/aplus-media-library-service-media/366b43aa-a099-4312-98fb-4e5effaaed61.__CR0,0,970,300_PT0_SX970_V1___.jpg'
},
{
  name: "luca",
  link: 'https://cartelescine.files.wordpress.com/2021/02/lucabanner.jpg'
 },
 {
   name: "mandalorian",
   link: 'https://pbs.twimg.com/media/EHRv5d_UUAEMUEk.jpg'
 },
 {
   name: "jeffgoldblum",
   link: 'https://conlagentenoticias.com/wp-content/uploads/2020/11/jeff-goldblum.jpg'
 }      
  ];

  const delay = 2500;

const SlideshowBanner = () => {
    
    
      const [index, setIndex] = useState(0);
      const timeoutRef = useRef(null);

      const navigate = useNavigate()
    
      function resetTimeout() {
        if (timeoutRef.current) {
          clearTimeout(timeoutRef.current);
        }
      }
    
      useEffect(() => {
        resetTimeout();
        timeoutRef.current = setTimeout(
          () =>
            setIndex((prevIndex) =>
              prevIndex === slideImages.length - 1 ? 0 : prevIndex + 1
            ),
          delay
        );
    
        return () => {
          resetTimeout();
        };
      }, [index]);
    
      return (
        <div className="slideshow">
          <div
            className="slideshowSlider"
            style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
          >
            {slideImages.map((obj, index) => (
              <div
                className="slide"
                key={index}
              >
               
                  <img
                   className="slide"
                   key={index}
                   onClick={()=>navigate(`/home/${obj.name}`)}
                  src={obj.link}
                  alt="new releases"
                  />
                
              </div>
            ))}
          </div>
    
          <div className="slideshowDots">
            {slideImages.map((_, idx) => (
              <div
                key={idx}
                className={`slideshowDot${index === idx ? " active" : ""}`}
                onClick={() => {
                  setIndex(idx);
                }}
              ></div>
            ))}
          </div>
        </div>
      );
}

export default SlideshowBanner