import React from "react";
import { useNavigate } from "react-router-dom";
import User from "./User";

const Navbar = (props) => {

  const navigate = useNavigate()
    return (
      <div style={styles.navStyles}>
          <img onClick={()=> navigate('/home')} className="logo" alt="disneylogo" src="https://cnbl-cdn.bamgrid.com/assets/7ecc8bcb60ad77193058d63e321bd21cbac2fc67281dbd9927676ea4a4c83594/original"></img>
        <p
          style={styles.default}
          onClick={()=> navigate('/home')}
        >
          INICIO
        </p>
  
        <p
          style={styles.default}
          onClick={()=> navigate("/home/search")}
        >
          BÚSQUEDA
        </p>

        <p
          style={styles.default}
          onClick={()=> navigate("/home/watchlist")}
        >
          MI LISTA
        </p>

        <p
          style={styles.default}
          onClick={()=> navigate("/home/original")}
        >
          ORIGINALES
        </p>

        <p
          style={styles.default}
          onClick={()=> navigate("/home/movie")}
        >
          PELÍCULAS
        </p>

        <p
          style={styles.default}
          onClick={()=> navigate("/home/serie")}
        >
          SERIES
        </p>

        <User setUser={props.setUser} />
      </div>
    );
  };

  
  export default Navbar;

  const styles = {
    navStyles: {
      display: "flex",
      position: 'sticky',
      zIndex: '1',
      top: '0px',
      backgroundColor: '#0c111b',
      alignItems: "center",
      justifyContent: "space-around",
      marginBottom: "20px"
    },
    
    default: {
      color: "white",
      textDecoration: 'none'
    }
  };